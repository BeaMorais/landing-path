import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/app.js';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import rootReducer from './reducers/rootReducers';

const store = createStore(rootReducer);

ReactDOM.render(<Provider store={store}><App /></Provider>, 
    document.getElementById('app')
    )