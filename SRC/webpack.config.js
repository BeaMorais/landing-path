const path = require ('path');
const HtmlWebpackPlugin = require ('html-webpack-plugin')

module.exports = {
    entry: './app/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'index_bundle.js'
    },
    module:{
        rules:[{
            loader: 'babel-loader',
            test: '/\.js$/',
            exclude: /node_modules/
        }]
    },
    mode: 'development',
    plugins: 
    ["react-hot-loader/babel", 
            new HtmlWebpackPlugin ({
            template: 'app/index.html'
        })
    ],
    devServer: {
        contentBase: "./SRC",
        publicPath: "/",
        hot: true
    }   
}